import java.util.Date;

public class Member extends Person {

	private String memberNo;
	private static int currentMemId = 0;

	/**
	 * 
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 */
	public Member(String name, String phoneNo, Date birthDate,
        String address, Sex sex, String postalCode)
        throws InvalidFormatException {
        
        super(name, phoneNo, birthDate, address, sex, postalCode);
        memberNo = generateMemberNo();
	}

	public String getMemberNo() {
		return memberNo;
	}

	private String generateMemberNo() {
		
        String memNo = "" + (currentMemId++);
        
        switch (memNo.length()) {
            
            case 1  : return "00000000" + memNo;
            case 2  : return "0000000" + memNo;
            case 3  : return "000000" + memNo;
            case 4  : return "00000" + memNo;
            case 5  : return "0000" + memNo;
            case 6  : return "000" + memNo;
            case 7  : return "00" + memNo;
            case 8  : return "0" + memNo;
            default : return memNo;
        }
	}
}