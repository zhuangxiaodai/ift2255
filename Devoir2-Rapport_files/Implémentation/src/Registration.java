import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class Registration {

	private String codeService;
	private String codeMem;
	private String codeProf;
	private Date dateNow;
	private Date dateService;
	private String comment;

    public static boolean verifyRegistration(String codeService,
        String codeMem, String codeProf) {
        
        // On ne connaît pas les directives de vérification d'inscription
        
        if (Math.random() < 0.5) {
            
            return true;
        }
        
        return false;
    }

	/**
	 * 
	 * @param codeService
	 * @param codeMem
	 * @param codeProf
	 * @param comment
	 */
	public Registration(String codeService, String codeMem, String codeProf,
        String comment) throws ParseException {
		
        this.codeService = codeService;
        this.codeMem = codeMem;
        this.codeProf = codeProf;
        this.comment = comment;
        
        SimpleDateFormat f1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat f2 = new SimpleDateFormat("dd-MM-yyyy");
        dateNow = f1.parse(f1.format(new Date(System.currentTimeMillis())));
        
        // Service fourni le jour même
        dateService = f2.parse(f2.format(new Date(System.currentTimeMillis())));
	}

	public String getCodeService() {
		return this.codeService;
	}

	public String getCodeMem() {
		return this.codeMem;
	}

	public String getCodeProf() {
		return this.codeProf;
	}

	public Date getDateNow() {
		return this.dateNow;
	}

	public Date getDateService() {
		return this.dateService;
	}

	public String getComment() {
		return this.comment;
	}

	/**
	 * 
	 * @param comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

}