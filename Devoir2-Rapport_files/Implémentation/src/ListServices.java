import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class ListServices {

	private static ArrayList<Service> services;

	public static ArrayList<Service> getServices() {
		return services;
	}

	/**
	 * 
	 * @param code
	 */
	public static Service getServiceByCode(String code) {
		
        for (Service s : services) {
            
            if (s.getCode().equals(code)) {
                
                return s;
            }
        }
        
        return null;
	}

	public static ArrayList<Service> getServicesToday() {
		
        ArrayList<Service> servicesToday = new ArrayList<Service>();
        
        Day day = Day.valueOf((String)
        (new SimpleDateFormat("EEEE")).format(System.currentTimeMillis()));
        
        for (Service s : services) {
            
            Day[] days = s.getOccurrences();
            
            for (int i = 0; i < days.length; i++) {
                
                if (days[i].equals(day)) {
                    
                    servicesToday.add(s);
                    break;
                }
            }
        }
        
        return servicesToday;
	}

	/**
     * Creates a new service and adds it onto list of services
	 * 
	 * @param name Name of service
	 * @param startDate Start date of service
	 * @param endDate End date of service
	 * @param occurrences
	 * @param capacityMax
	 * @param comment
	 * @param price
	 * @param serviceTime
	 * @param profNo
	 */
	public static void createService(String name, Date startDate, Date endDate,
        Day[] occurrences, int capacityMax, String comment, double price,
        Date serviceTime, String profNo) throws InvalidFormatException {
		
        try {
            
            services.add(new Service(name, startDate, endDate, occurrences,
            capacityMax, comment, price, serviceTime, profNo));
        } catch(ParseException e) {
            
            System.out.println("Parse Exception");
        }
	}

	/**
	 * 
	 * @param codeService
	 * @param name
	 * @param startDate
	 * @param endDate
	 * @param occurences
	 * @param capacityMax
	 * @param comment
	 * @param price
	 * @param serviceTime
	 * @param profNo
	 */
	public static String modifyService(String codeService, String name,
        Date startDate, Date endDate, Day[] occurrences, int capacityMax,
        String comment, Double price, Date serviceTime,
        String profNo) throws InvalidFormatException {
		
        Service service = getServiceByCode(codeService);
        
        if (service != null) {
            
            Service.verifyService(name, startDate, endDate, occurrences,
            capacityMax, price, profNo);
            
            service.setName(name);
            service.setStartDate(startDate);
            service.setEndDate(endDate);
            service.setOccurrences(occurrences);
            service.setCapacityMax(capacityMax);
            service.setComment(comment);
            service.setPrice(price);
            service.setServiceTime(serviceTime);
            service.setProfNo(profNo);
            
            return "Modification complète";
            
        } else {
            
            return "Service non disponible";
        }
	}

	/**
	 * 
	 * @param code
	 */
	public static String deleteService(String code) {
		
        Service service = getServiceByCode(code);
        
        if (service != null) {
            
            if (ListValidation.isValidationExisting(code)) {
                
                return "Service ne peut pas être supprimé à ce moment";
            } else {
                
                AccountingUtils.removeProvidedProf(service.getProfNo());
                return "Service supprimé";
            }
        } else {
            
            return "Service non trouvé";
        }
	}

	/**
	 * 
	 * @param codeProf
	 */
	public static void deleteServicesProf(String codeProf) {
		
        for (Service s : services) {
            
            if (s.getProfNo().equals(codeProf)) {
                
                services.remove(s);
            }
        }
	}

}