import java.util.ArrayList;
import java.text.ParseException;

public class ListRegistrations {

	private static ArrayList<Registration> registrations;

	public static ArrayList<Registration> getRegistrations() {
		
        return registrations;
	}

	/**
	 * 
	 * @param registration
	 */
	public static void addRegistration(Registration registration) {
		
        registrations.add(registration);
	}

	/**
	 * 
	 * @param codeMember
	 * @param codeService
	 */
	public static void removeRegistration(String codeMember,
        String codeService) {
		
        for (Registration r : registrations) {
            
            if (r.getCodeMem().equals(codeMember)
                && r.getCodeService().equals(codeService)) {
                
                registrations.remove(r);
                return;
            }
        }
	}

	public static void removeAllRegistrations() {
		
        registrations.clear();
	}

	/**
	 * 
	 * @param codeService
	 * @param codeMem
	 * @param codeProf
	 * @param comment
	 */
	public static Registration createRegistration(String codeService,
        String codeMem, String codeProf, String comment) {
        
        if (Registration.verifyRegistration(codeService, codeMem, codeProf)) {
            
            try {
                
                Registration reg = new Registration(codeService, codeMem, codeProf,
                comment);
                
                registrations.add(reg);
                
                return reg;
            } catch(ParseException e) {
                
                return null;
            }
        } else {
            
            return null;
        }
	}

	/**
	 * 
	 * @param codeMem
	 * @param codeService
	 */
	public static Registration findExistingRegistration(String codeMem,
        String codeService) {
		
        for (Registration r : registrations) {
            
            if (r.getCodeMem().equals(codeMem)
                && r.getCodeService().equals(codeService)) {
                
                return r;
            }
        }
        
        return null;
	}

	/**
	 * 
	 * @param codeService
	 */
	public static boolean containsRegistration(String codeService) {
		
        for (Registration r : registrations) {
            
            if (r.getCodeService().equals(codeService)) {
                
                return true;
            }
        }
        
        return false;
	}

	/**
	 * 
	 * @param codeProf
	 */
	public static void removeRegistrationsProf(String codeProf) {
		
        for (Registration r : registrations) {
            
            if (r.getCodeProf().equals(codeProf)) {
                
                registrations.remove(r);
                return;
            }
        }
	}

	/**
	 * 
	 * @param codeMem
	 */
	public static void removeRegistrationsMem(String codeMem) {
		
        for (Registration r : registrations) {
            
            if (r.getCodeMem().equals(codeMem)) {
                
                registrations.remove(r);
                return;
            }
        }
	}
    
    public static boolean confirmRegistration(String memberNo,
        String serviceNo, String comments) {
        
        Registration reg = findExistingRegistration(memberNo, serviceNo);
        
        if (reg != null) {
            
            ListValidation.logValidation(reg.getCodeProf(), memberNo,
            serviceNo, comments);
            
            return true;
        } else {
            
            return false;
        }
    }
}