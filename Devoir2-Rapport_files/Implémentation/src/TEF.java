public class TEF {

	private String nameProf;
	private String codeProf;
	private double weeklyProfRevenueAmount;

	/**
	 * 
	 * @param nameProf
	 * @param codeProf
	 * @param weeklyProfRevenueAmount
	 */
	public TEF(String nameProf, String codeProf,
        double weeklyProfRevenueAmount) {
		
        this.nameProf = nameProf;
        this.codeProf = codeProf;
        this.weeklyProfRevenueAmount = weeklyProfRevenueAmount;
	}

	public void outputTEFFile() {
		
        System.out.println("Generated TEF file");
	}

	public String getNameProf() {
		return this.nameProf;
	}

	/**
	 * 
	 * @param nameProf
	 */
	public void setNameProf(String nameProf) {
		this.nameProf = nameProf;
	}

	public String getCodeProf() {
		return this.codeProf;
	}

	public double getWeeklyProfRevenueAmount() {
		return this.weeklyProfRevenueAmount;
	}

	/**
	 * 
	 * @param weeklyProfRevenueAmount
	 */
	public void setWeeklyProfRevenueAmount(double weeklyProfRevenueAmount) {
		this.weeklyProfRevenueAmount = weeklyProfRevenueAmount;
	}

}