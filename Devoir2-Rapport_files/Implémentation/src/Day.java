public enum Day {
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday;
    
    private static final Day[] copyOfValues = values();
    
    public static boolean contains(Day day) {
        
        for (Day d : copyOfValues) {
            
            if (d.equals(day)) {
                
                return true;
            }
        }
        return false;
    }
}