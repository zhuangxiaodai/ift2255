import java.util.Date;

public class Professional extends Person {

	private String profNo;
	private static int currentProfId = 0;

	/**
	 * 
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 */
	public Professional(String name, String phoneNo, Date birthDate,
        String address, Sex sex, String postalCode)
        throws InvalidFormatException {

        super(name, phoneNo, birthDate, address, sex, postalCode);
        profNo = generateProfNo();
	}

	public String getProfNo() {
		return this.profNo;
	}

	private String generateProfNo() {
		
        String proNo = "" + (currentProfId++);
        
        switch (proNo.length()) {
            
            case 1  : return "00000000" + proNo;
            case 2  : return "0000000" + proNo;
            case 3  : return "000000" + proNo;
            case 4  : return "00000" + proNo;
            case 5  : return "0000" + proNo;
            case 6  : return "000" + proNo;
            case 7  : return "00" + proNo;
            case 8  : return "0" + proNo;
            default : return proNo;
        }
	}
}