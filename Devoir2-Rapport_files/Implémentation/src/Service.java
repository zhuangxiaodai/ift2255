import java.util.Arrays;
import java.util.Date;

import java.text.SimpleDateFormat;
import java.text.ParseException;

public class Service {

	private String name;
    private Date dateNow;
	private Date startDate;
	private Date endDate;
	private Day[] occurrences;
	private String code;
	private int capacityMax;
	private int capacityNow;
	private String comment;
	private double price;
	private Date serviceTime;
	private String profNo;

    private static int currentServiceId = 0;

	/**
	 * 
	 * @param name
	 * @param startDate
	 * @param endDate
	 * @param occurrences
	 * @param capacityMax
	 * @param price
	 * @param profNo
	 */
	public static void verifyService(String name, Date startDate, Date endDate,
        Day[] occurrences, int capacityMax, double price, String profNo)
        throws InvalidFormatException {
		
        if (! name.matches("[A-Z][a-z]*")) {
            
            throw new InvalidFormatException("name");
        }
        
        if (! startDate.before(new Date(System.currentTimeMillis()))) {
            
            throw new InvalidFormatException("startDate");
        }
        
        if (! endDate.after(startDate)) {
            
            throw new InvalidFormatException("endDate");
        }
        
        for (int i = 0; i < occurrences.length; i++) {
            
            if (! Day.contains(occurrences[i])) {
                
                throw new InvalidFormatException("occurences");
            }
        }
        
        if (! (capacityMax >= 0 && capacityMax <= 30)) {
            
            throw new InvalidFormatException("capacityMax");
        }
        
        if (! (price >= 0 && price <= 100)) {
            
            throw new InvalidFormatException("price");
        }
        
        if (! (profNo.length() == 9)) {
            
            throw new InvalidFormatException("profNo");
        }
	}

	/**
	 * 
	 * @param name
	 * @param startDate
	 * @param endDate
	 * @param occurrences
	 * @param capacityMax
	 * @param comment
	 * @param price
	 * @param serviceTime
	 * @param profNo
	 */
	public Service(String name, Date startDate, Date endDate,
        Day[] occurrences, int capacityMax, String comment, double price,
        Date serviceTime, String profNo) throws ParseException,
        InvalidFormatException {
        
        verifyService(name, startDate, endDate, occurrences, capacityMax,
        price, profNo);
        
        SimpleDateFormat f1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        SimpleDateFormat f2 = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat f3 = new SimpleDateFormat("HH-mm");
        
        this.name = name;
        this.dateNow = f1.parse(f1.format(new Date(System.currentTimeMillis())));
        this.startDate = f2.parse(f2.format(startDate));
        this.endDate = f2.parse(f2.format(endDate));
        this.occurrences = occurrences;
        code = generateServiceNo();
        this.capacityMax = capacityMax;
        capacityNow = 0;
        this.comment = comment;
        this.price = price;
        this.serviceTime = f3.parse(f3.format(serviceTime));
        this.profNo = profNo;
	}

    private String generateServiceNo() {
		
        String servNo = "" + (currentServiceId++);
        
        switch (servNo.length()) {
            
            case 1  : return "000000" + servNo;
            case 2  : return "00000" + servNo;
            case 3  : return "0000" + servNo;
            case 4  : return "000" + servNo;
            case 5  : return "00" + servNo;
            case 6  : return "0" + servNo;
            default : return servNo;
        }
	}

	public String getName() {
		return this.name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * 
	 * @param endDate
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Day[] getOccurrences() {
		return occurrences;
	}

	/**
	 * 
	 * @param occurrences
	 */
	public void setOccurrences(Day[] occurrences) {
        
		this.occurrences = occurrences;
	}

	public String getCode() {
		return this.code;
	}

	public int getCapacityMax() {
		return this.capacityMax;
	}

	/**
	 * 
	 * @param capacityMax
	 */
	public void setCapacityMax(int capacityMax) {
		this.capacityMax = capacityMax;
	}

	public int getCapacityNow() {
		return this.capacityNow;
	}

    public void setCapacityNow(int capacityNow) {
        
        this.capacityNow = capacityNow;
    }

	public int getRemainCapacity() {
		// TODO - implement Service.getRemainCapacity
		throw new UnsupportedOperationException();
	}

	public String getComment() {
		return this.comment;
	}

	/**
	 * 
	 * @param comment
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	public double getPrice() {
		return this.price;
	}

	/**
	 * 
	 * @param price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	public Date getServiceTime() {
		return this.serviceTime;
	}

	/**
	 * 
	 * @param serviceTime
	 */
	public void setServiceTime(Date serviceTime) {
		this.serviceTime = serviceTime;
	}

	public String getProfNo() {
		return this.profNo;
	}

	/**
	 * 
	 * @param profNo
	 */
	public void setProfNo(String profNo) {
		this.profNo = profNo;
	}
    
    public String toString() {
        
        return "Name - " + name + "\n" +
               "Start Date - " + startDate.toString() + "\n" +
               "End Date - " + endDate.toString() + "\n" +
               "Occurences - " + Arrays.toString(occurrences) + "\n" +
               "Service code - " + code + "\n" +
               "Max capacity - " + capacityMax + "\n" +
               "Current capacity - " + capacityNow + "\n" +
               "Remaining capacity - " + getRemainCapacity() + "\n" +
               "Price - " + price + "\n" +
               "Service Time - " + serviceTime.toString() + "\n" +
               "Professional number - " + profNo + "\n" +
               "Comment - " + comment;
    }
}