import java.util.ArrayList;
import java.text.ParseException;

public class ListValidation {

	private static ArrayList<Validation> validations;

	/**
	 * 
	 * @param codeProf
	 * @param codeMem
	 * @param codeService
	 * @param comments
	 */
	public static void logValidation(String codeProf, String codeMem,
        String codeService, String comments) {
		
        try {
            
            validations.add(new Validation(codeMem, codeProf, codeService,
            comments));
        } catch(Exception e) {
            
            System.out.println("Parse exception");
        }
	}

	public static ArrayList<Validation> getValidation() {
		
        return validations;
	}

	/**
	 * 
	 * @param codeService
	 */
	public static boolean isValidationExisting(String codeService) {
		
        for (Validation v : validations) {
            
            if (v.getCodeService().equals(codeService)) {
                
                return true;
            }
        }
        
        return false;
	}

	public static void clear() {
		
        validations.clear();
	}
}