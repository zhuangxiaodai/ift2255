import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AccountingUtils {

	private static HashMap<String, Double> servicesBalance;
	private static HashMap<String, ArrayList<String>> providedProfs;

    /**
     * Generates a TEF file from a TEF object
     */
	public static void generateTEFFile() {
		
		System.out.println("Generated TEF file");
	}

	/**
	 * Calculates a professional's income for the week
     *
	 * @param codeProf Professional's id number
	 */
	public static double calculateProfWeeklyRevenue(String codeProf) {
		
        double profit = 0;
        ArrayList<String> services = providedProfs.get(codeProf);
        
        for (String code : services) {
            
            profit += servicesBalance.get(code);
        }
        
        return profit;
	}

    /**
     * Generates a services report for the week
     */

	public static void generateWeeklyServicesReport() {
		
        // Generated weekly services report
	}

	public static HashMap<String, Double> getServicesBalance() {
        
		return servicesBalance;
	}

	/**
	 * Retrieves balance associated with given service code
     *
	 * @param code Service code
	 */
	public static Double getBalance(String code) {
		
        return servicesBalance.get(code);
	}

    /**
     * Calculates total balance of all services
     */

	public static double getTotalBalance() {
		
        double revenue = 0;
        
        for (Map.Entry e : servicesBalance.entrySet()) {
            
            revenue = Double.sum(revenue, (double) e.getValue());
        }
        
        return revenue;
	}

	public static void resetAllBalance() {
		
        servicesBalance.clear();
	}

	/**
	 * Adds an entry onto the service balance hashmap
     *
	 * @param code Service code key
	 * @param price Income obtained from service value
	 */
	public static void addServiceBalance(String code, double price) {
		
        servicesBalance.put(code, price);
	}

	/**
	 * Sets the balance for a given service code
     *
	 * @param code Service code key
	 * @param price Updated income from service value
	 */
	public static void setBalance(String code, double price) {
		
        servicesBalance.replace(code, servicesBalance.get(code), price);
	}

	/**
	 * Removes a given service's entry from the service balance hashmap
     *
	 * @param code Service code key
	 */
	public static void removeServiceBalance(String code) {
		
        servicesBalance.remove(code);
	}

	public static HashMap<String, ArrayList<String>> getProvidedProfs() {
        
		return providedProfs;
	}

	/**
     * Adds a service offered by the given professional or creates a new entry
     * if professional offered none before
	 *
	 * @param code Professional code key
	 * @param serviceCode Service code value
	 */
	public static void addProvidedProfs(String code, String serviceCode) {
		
        ArrayList<String> services = providedProfs.get(code);
        
        if (services == null) {
            
            services = new ArrayList<String>();
        }
        
        services.add(serviceCode);
        providedProfs.put(code, services);
	}

	/**
	 * Retrieves list of services offered by given professional
     *
	 * @param code Professional code key
	 */
	public static ArrayList<String> getServicesProvided(String code) {
		
        return providedProfs.get(code);
	}

	/**
	 * Removes services associated with given professional
     *
	 * @param code Professional code key
	 */
	public static void removeProvidedProf(String code) {
		
        providedProfs.remove(code);
	}

	/**
	 * Removes a specific service associated with given professional
     *
	 * @param codeProf
	 * @param codeService
	 */
	public static void removeProvidedProfService(String codeProf,
        String codeService) {
		
        ArrayList<String> services = providedProfs.get(codeProf);
        services.remove(codeService);
	}
}