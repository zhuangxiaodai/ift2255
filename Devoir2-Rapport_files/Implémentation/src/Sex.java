public enum Sex {
	Male,
	Female,
	Other;
    
    private static final Sex[] copyOfValues = values();
    
    public static boolean contains(Sex sex) {
        
        for (Sex s : copyOfValues) {
            
            if (s.equals(sex)) {
                
                return true;
            }
        }
        return false;
    }
}