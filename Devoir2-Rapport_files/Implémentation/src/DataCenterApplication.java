import java.util.Date;
import java.util.ArrayList;

public class DataCenterApplication {

    private static void accountingProcedure() {
		
        AccountingUtils.generateTEFFile();
        AccountingUtils.generateWeeklyServicesReport();
	}

	public static void showMessage(String message) {
		
        System.out.println(
        "\n\t********************************************\n\t"
        + message + "\n\t********************************************");
	}

	public static void generateWeeklyServicesReport() {
		
        AccountingUtils.generateWeeklyServicesReport();
        showMessage("Generated Weekly Services Report");
	}

	/**
	 * 
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 * @param type
	 */
	public static void createPerson(String name, String phoneNo,
        Date birthDate, String address, Sex sex, String postalCode,
        Type type) {
		
        if (type.equals(Type.Member)) {
            
            try {
                
                Member member = ListPersons.createMember(name, phoneNo,
                birthDate, address, sex, postalCode);
                showMessage("Membre créé : " + member.getMemberNo());
            } catch(InvalidFormatException e) {
                
                showMessage("Format invalide pour attribut : " +
                e.getMessage());
            }
        } else if (type.equals(Type.Professional)) {
            
            try {
                
                Professional prof = ListPersons.createProf(name, phoneNo,
                birthDate, address, sex, postalCode);
                showMessage("Professionnel créé : " + prof.getProfNo());
            } catch(InvalidFormatException e) {
                
                showMessage("Format invalide pour attribut : " +
                e.getMessage());
            }
        } else {
            
            showMessage("Type invalide : " + type);
        }
	}

	/**
	 * 
	 * @param code
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 * @param type
	 */
	public static void modifyPerson(String code, String name, String phoneNo,
        Date birthDate, String address, Sex sex, String postalCode,
        Type type) {
        
        if (type.equals(Type.Member)) {
            
            try {
                
                showMessage(ListPersons.modifyMember(code, name, phoneNo,
                birthDate, address, sex, postalCode));
            } catch(InvalidFormatException e) {
                
                showMessage("Format invalide pour attribut : " +
                e.getMessage());
            }
        } else if (type.equals(Type.Professional)) {
            
            try {
                
                showMessage(ListPersons.modifyProf(code, name, phoneNo,
                birthDate, address, sex, postalCode));
            } catch(InvalidFormatException e) {
                
                showMessage("Format invalide pour attribut : " +
                e.getMessage());
            }
        } else {
            
            showMessage("Type invalide : " + type);
        }
	}

	/**
	 * 
	 * @param code
	 * @param type
	 */
	public static void deletePerson(String code, Type type) {
		
        if (type.equals(Type.Member)) {
            
            showMessage(ListPersons.deleteMember(code));
        } else if (type.equals(Type.Professional)) {
            
            showMessage(ListPersons.deleteProf(code));
        } else {
            
            showMessage("Type invalide : " + type);
        }
	}

	/**
	 * 
	 * @param name
	 * @param startDate
	 * @param endDate
	 * @param occurrences
	 * @param capacityMax
	 * @param capacityMin
	 * @param comment
	 * @param price
	 * @param serviceTime
	 * @param profNo
	 */
	public static void createService(String name, Date startDate, Date endDate,
        Day[] occurrences, int capacityMax, String comment, double price,
        Date serviceTime, String profNo) {
		
        try {
            
            ListServices.createService(name, startDate, endDate, occurrences,
            capacityMax, comment, price, serviceTime, profNo);
            showMessage("Service créé");
        } catch(InvalidFormatException e) {
            
            showMessage("Format invalide pour attribut : " + e.getMessage());
        }
	}

	/**
	 * 
	 * @param codeService
	 * @param name
	 * @param startDate
	 * @param endDate
	 * @param occurrences
	 * @param capacityMax
	 * @param capacityMin
	 * @param comment
	 * @param price
	 * @param serviceTime
	 * @param profNo
	 */
	public static void modifyService(String codeService, String name,
        Date startDate, Date endDate, Day[] occurrences, int capacityMax,
        String comment, double price, Date serviceTime, String profNo) {
		
        try {
            
            showMessage(ListServices.modifyService(codeService, name,
            startDate, endDate, occurrences, capacityMax, comment, price,
            serviceTime, profNo));
        } catch(InvalidFormatException e) {
            
            showMessage("Format invalide pour attribut : " + e.getMessage());
        }
	}

	/**
	 * 
	 * @param codeService
	 */
	public static void deleteService(String codeService) {
		
        showMessage(ListServices.deleteService(codeService));
	}

	public static void getServicesToday() {
		
        ArrayList<Service> services = ListServices.getServicesToday();
        
        if (services != null) {
            
            showMessage("Services disponibles : \n" + services.toString());
        } else {
            
            showMessage("Aucun service disponible");
        }
	}

	/**
	 * 
	 * @param codeMem
	 * @param codeService
	 */
	public static void confirmRegistration(String codeMem, String codeService,
        String comments) {
		
        if (ListRegistrations.confirmRegistration(codeMem, codeService,
            comments)) {
            
            showMessage("Accès autorisé");
        } else {
            
            showMessage("Accès refusé");
        }
	}

	/**
	 * 
	 * @param code
	 */
	public static void validateMember(String code) {
		
        if (ListPersons.containsMember(code)) {
            
            showMessage("Membre valide");
        } else {
            
            showMessage("Membre invalide");
        }
	}

	/**
	 * 
	 * @param codeMember
	 * @param codeService
	 */
	public static void registerSession(String codeMember, String codeService,
        String comment) {
		
        Service service = ListServices.getServiceByCode(codeService);
        
        if (service != null) {
            
            int cap = service.getRemainCapacity();
            
            if (cap > 0) {
                
                Registration reg = ListRegistrations.createRegistration(
                codeService, codeMember, service.getProfNo(), comment);
                
                if (reg != null) { // Validation valide
                    
                    service.setCapacityNow(service.getCapacityNow() + 1);
                    AccountingUtils.addServiceBalance(codeService,
                    service.getPrice());
                    AccountingUtils.addProvidedProfs(service.getProfNo(),
                    codeService);
                    
                    showMessage("Inscription complète");
                } else { // Validation invalide
                    
                    showMessage("Information invalide");
                }
            } else {
                
                showMessage("Capacité complète");
            }
        } else {
            
            showMessage("Service non disponible");
        }
	}
}