import java.util.Date;
import java.util.HashMap;


public class ListPersons {

	private static HashMap<String, Member> members;
	private static HashMap<String, Professional> professionals;

	/**
	 * 
	 * @param code
	 */
	public static boolean containsMember(String code) {
		
        return members.containsKey(code);
	}

	/**
	 * 
	 * @param code
	 */
	public static boolean containsProf(String code) {
		
        return professionals.containsKey(code);
	}

	public static HashMap<String, Member> getMembers() {
        
		return members;
	}

	/**
	 * 
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 */
	public static Member createMember(String name, String phoneNo,
        Date birthDate, String address, Sex sex, String postalCode)
        throws InvalidFormatException {
		
        Member member = new Member(name, phoneNo, birthDate, address, sex,
        postalCode);
        
        members.put(member.getMemberNo(), member);
        
        return member;
	}

	/**
	 * 
	 * @param code
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 */
	public static String modifyMember(String code, String name, String phoneNo,
    Date birthDate, String address, Sex sex, String postalCode)
    throws InvalidFormatException {
		
        Person.verifyFormat(name, phoneNo, birthDate, address, sex,
        postalCode);
        
        Member member = members.get(code);
        
        member.setName(name);
        member.setPhoneNo(phoneNo);
        member.setBirthDate(birthDate);
        member.setAddress(address);
        member.setSex(sex);
        member.setPostalCode(postalCode);
        
        return "Modifications apportées";
	}

	/**
	 * 
	 * @param code
	 */
	public static String deleteMember(String code) {
		
        if (members.containsKey(code)) {
            
            ListRegistrations.removeRegistrationsMem(code);
            members.remove(code);
            return "Membre supprimé";
        } else {
            
            return "Numéro de membre invalide";
        }
	}

	public static HashMap<String, Professional> getProfs() {
		
        return professionals;
	}

	/**
	 * 
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 */
	public static Professional createProf(String name, String phoneNo,
        Date birthDate, String address, Sex sex, String postalCode)
        throws InvalidFormatException {
		
        Professional prof = new Professional(name, phoneNo, birthDate,
        address, sex, postalCode);
        
        professionals.put(prof.getProfNo(), prof);
        
        return prof;
	}

	/**
	 * 
	 * @param code
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 */
	public static String modifyProf(String code, String name, String phoneNo,
        Date birthDate, String address, Sex sex, String postalCode)
        throws InvalidFormatException {
		
        Person.verifyFormat(name, phoneNo, birthDate, address, sex,
        postalCode);
        
        Professional prof = professionals.get(code);
        
        prof.setName(name);
        prof.setPhoneNo(phoneNo);
        prof.setBirthDate(birthDate);
        prof.setAddress(address);
        prof.setSex(sex);
        prof.setPostalCode(postalCode);
        
        return "Modifications apportées";
	}

	/**
	 * 
	 * @param code
	 */
	public static String deleteProf(String code) {
		
        if (professionals.containsKey(code)) {
            
            ListRegistrations.removeRegistrationsProf(code);
            professionals.remove(code);
            return "Professionel supprimé";
        } else {
            
            return "Numéro de profesionnel invalide";
        }
	}
}