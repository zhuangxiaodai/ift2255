import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

public class Validation {

	private String profNo;
	private String memberNo;
	private String comments;
	private String codeNo;
	private Date timeStamp;

	/**
	 * 
	 * @param memberNo
	 * @param profNo
	 * @param codeNo
	 * @param comments
	 */
	public Validation(String memberNo, String profNo, String codeNo,
        String comments) throws ParseException {
		
        this.memberNo = memberNo;
        this.profNo = profNo;
        this.codeNo = codeNo;
        this.comments = comments;
        
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        this.timeStamp = f.parse(f.format(new Date(System.currentTimeMillis())));
	}

	public String getMemberNo() {
		return this.memberNo;
	}

	public String getComments() {
		return this.comments;
	}

	/**
	 * 
	 * @param comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getCodeService() {
		return this.codeNo;
	}

	public Date getTimeStamp() {
		return this.timeStamp;
	}

	public String getProfNo() {
		
        return profNo;
	}
}