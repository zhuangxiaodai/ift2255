import java.util.Scanner;

public class EntryPoint {

	/**
	 * 
	 * @param cmd
	 */
	public static void processIncomingCommand(String cmd) {
		
        switch (cmd) {
            
            case "Generate Weekly Services Report" : {
                
                DataCenterApplication.generateWeeklyServicesReport();
                break;
            }
            
            case "Create Person" : {
                
                DataCenterApplication.showMessage("Person created");
                break;
            }
            
            case "Modify Person" : {
                
                DataCenterApplication.showMessage("Person modified");
                break;
            }
            
            case "Delete Person" : {
                
                DataCenterApplication.showMessage("Person deleted");
                break;
            }
            
            case "Create Service" : {
                
                DataCenterApplication.showMessage("Service created");
                break;
            }
            
            case "Modify Service" : {
                
                DataCenterApplication.showMessage("Service modified");
                break;
            }
            
            case "Delete Service" : {
                
                DataCenterApplication.showMessage("Service deleted");
                break;
            }
            
            case "Confirm Registration" : {
                
                DataCenterApplication.showMessage("Registration confirmed");
                break;
            }
            
            case "Validate Member" : {
                
                DataCenterApplication.showMessage("Member validated");
                break;
            }
            
            case "Register Session" : {
                
                DataCenterApplication.showMessage("Session registered");
                break;
            }
            
            case "Exit" : {
            
                DataCenterApplication.showMessage("A la prochaine");
                System.exit(0);
            }
            
            default : {
                
                DataCenterApplication.showMessage("Commande invalide");
            }
        }
        
        main(null);
	}

    public static void main(String[] args) {
        
        System.out.println("\nVeuillez entrez une des commandes ci-suit: ");
        System.out.println("\tGenerate Weekly Services Report");
        System.out.println("\tCreate Person");
        System.out.println("\tModify Person");
        System.out.println("\tDelete Person");
        System.out.println("\tCreate Service");
        System.out.println("\tModify Service");
        System.out.println("\tDelete Service");
        System.out.println("\tConfirm Registration");
        System.out.println("\tValidate Member");
        System.out.println("\tRegister Session");
        System.out.println("\tExit\n");
        
        System.out.print("Commande> ");
        Scanner sc = new Scanner(System.in);
        
        processIncomingCommand(sc.nextLine());
        sc.close();
    }
}