public class InvalidFormatException extends Exception {

	/**
	 * 
	 * @param errorMsg
	 */
	public InvalidFormatException(String errorMsg) {
		
        super(errorMsg);
	}
}