import java.util.Date;

public abstract class Person {

	private String name;
	private String phoneNo;
	private Date birthDate;
	private String address;
	private Sex sex;
	private String postalCode;
	private Status status;

	/**
	 * 
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 */
	public static void verifyFormat(String name, String phoneNo,
        Date birthDate, String address, Sex sex, String postalCode)
        throws InvalidFormatException {
		
        if (! name.matches("[A-Z][a-z]* [A-Z][a-z]*")) {
            
            throw new InvalidFormatException("name");
        }
        
        if (! phoneNo.matches("[0-9]{3}-[0-9]{3}-[0-9]{4}")) {
            
            throw new InvalidFormatException("phoneNo");
        }
        
        if (! birthDate.before(new Date(System.currentTimeMillis()))) {
            
            throw new InvalidFormatException("birthDate");
        }
        
        if (! address.matches("+(?: +){2}")) {
            
            throw new InvalidFormatException("address");
        }
        
        if (! Sex.contains(sex)) {
            
            throw new InvalidFormatException("sex");
        }
        
        if (! postalCode.matches("[A-Z][0-9][A-Z] [0-9][A-Z][0-9]")) {
            
            throw new InvalidFormatException("postalCode");
        }
	}

	/**
	 * 
	 * @param name
	 * @param phoneNo
	 * @param birthDate
	 * @param address
	 * @param sex
	 * @param postalCode
	 */
	public Person(String name, String phoneNo, Date birthDate, String address,
        Sex sex, String postalCode) throws InvalidFormatException {
		
        verifyFormat(name, phoneNo, birthDate, address, sex, postalCode);
        
        this.name = name;
        this.phoneNo = phoneNo;
        this.birthDate = birthDate;
        this.address = address;
        this.sex = sex;
        this.postalCode = postalCode;
        status = Status.Valid;
	}

	public String getName() {
		return this.name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	/**
	 * 
	 * @param phoneNo
	 */
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public Date getBirthDate() {
		return this.birthDate;
	}

    /**
	 * 
	 * @param birthDate
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getAddress() {
		return this.address;
	}

	/**
	 * 
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	public Sex getSex() {
		return sex;
	}

    /**
	 * 
	 * @param sex
	 */
	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	/**
	 * 
	 * @param postalCode
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public Status getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
}